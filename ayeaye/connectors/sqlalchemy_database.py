'''
Created on 22 Jan 2020

@author: si
'''
try:
    from sqlalchemy import create_engine
    from sqlalchemy.ext.declarative import declarative_base
    from sqlalchemy.orm import sessionmaker
    from sqlalchemy.pool import NullPool
    from sqlalchemy.dialects import postgresql
except:
    pass

from ayeaye.connectors.base import DataConnector, AccessMode
from ayeaye.pinnate import Pinnate

from collections import defaultdict


class SqlAlchemyDatabaseConnector(DataConnector):
    # Rows will be sent in batches to the database for models with large output
    AUTO_FLUSH_TRIGGER = 50000

    engine_type = ['sqlite://', 'mysql://', 'postgresql://']
    optional_args = {'schema_builder': None, 'ignore_duplicates': False}
    # TODO implement mysql

    def __init__(self, *args, **kwargs):
        """
        Connector to relational databases supported by SQLAlchemy.
        https://www.sqlalchemy.org/

        Supports SQLAlemy's ORM (Object Relational Mapper)

        For args: @see :class:`connectors.base.DataConnector`

        additional args for SqlalchemyDatabaseConnector
            schema_builder (optional) (callable) taking declarative base as the single argument.
            Must return a list of classes or single class with the shared declarative base.
            see https://docs.sqlalchemy.org/en/13/orm/extensions/declarative/api.html

            The behaviour of this connection will differ on single or list being passed.
            For example:
              .schema requires the name of the class in multiple mode.
              e.g.  my_connection.schema.OrmClass (for multiple mode)
                    and my_connection.schema (for 'single schema mode')

        Connection information-
            engine_url format varied with each engine
        e.g. sqlite:////data/sensors.db  for SQLite DB stored in '/data/sensors.db'
        """
        super().__init__(*args, **kwargs)

        if self.ignore_duplicates and not self.engine_url.startswith("postgresql"):
            raise ValueError('The ignore_duplicates argument is only supported on PostgreSQL currently')

        # on demand see :method:`connect`
        # the engine, declarative base class and sessions belong to this connection. Sharing these
        # between connections within a single model is not yet implemented.
        self.Base = None  # the declarative base
        self.session = None
        self.engine = None

        # self.schema_builder is built by init from the optional args
        self._schema_p = None  # see :method:`connect`

        # A staging area for new records not yet flushed to the database
        self.records = defaultdict(list)
        self.counter = 0

    def connect(self):

        if self.Base is None:
            self.Base = declarative_base()
            params = {}
            if self.engine_url.startswith("postgresql"):
                params["executemany_mode"] = "values"
                params["executemany_values_page_size"] = 10000
                params["executemany_batch_page_size"] = 500
            self.engine = create_engine(self.engine_url, poolclass=NullPool, **params)

            # Bind the engine to the metadata of the Base class so that the
            # declaratives can be accessed through a DBSession instance
            self.Base.metadata.bind = self.engine

            DBSession = sessionmaker(bind=self.engine)
            # A DBSession() instance establishes all conversations with the database
            # and represents a "staging zone" for all the objects loaded into the
            # database session object. Any change made against the objects in the
            # session won't be persisted into the database until you call
            # session.commit(). If you're not happy about the changes, you can
            # revert all of them back to the last commit by calling
            # session.rollback()
            self.session = DBSession()

            # initialise schema
            schema_classes = self.schema_builder(self.Base) \
                if self.schema_builder is not None else []

            if isinstance(schema_classes, list):
                as_dict = {c.__name__: c for c in schema_classes}
                self._schema_p = Pinnate(as_dict)
            else:
                self._schema_p = schema_classes  # single class

    def close_connection(self):
        # Ensure all rows have been written to the database
        self.flush()

        if self.session is not None:
            self.session.close()

    def __del__(self):
        """
        SqlAlchemy does it's own deconstruction.
        """
        pass

    def create_table_schema(self):
        """
        Create the tables defined in self.schema
        """
        if self.access == AccessMode.READ:
            raise ValueError("Can not build schema when access == READ")

        self.connect()
        self.Base.metadata.create_all(self.engine)

    def __len__(self):
        raise NotImplementedError("TODO")

    def __getitem__(self, key):
        raise NotImplementedError("TODO")

    def __iter__(self):
        """
        Generator for all records in all schema.
        """
        if self.access not in [AccessMode.READ, AccessMode.READWRITE]:
            raise ValueError("Can not read data without access == READ")

        schemata = [self.schema] if self.is_single_schema_mode else self.schema.values()
        for schema in schemata:
            # TODO take primary key from schema or default to 'id'
            for r in self.session.query(schema).order_by(schema.id).all():
                yield r

    @property
    def data(self):
        raise NotImplementedError("TODO")

    @property
    def schema(self):
        """
        SQLAlchemy's ORM classes represent tables in the underlying database.

        This property might be pushed up to :class:`DataConnector` so all Connectors could implement schemas.

        :returns: instance of :class:`AyeAye.pinnate` with names of the ORM classes as keys and the
        class (not instance) as the value.
        """
        self.connect()
        return self._schema_p

    @property
    def is_single_schema_mode(self):
        return not isinstance(self.schema, Pinnate)

    def add(self, item, model=None):
        """
        @param item: (dict) - data for this row
        @param model: (model) - when not in single schema mode
        """
        # Items must come as a dictionary of values
        if not isinstance(item, dict):
            raise ValueError('The item argument must be a dictionary')

        # You can't specify a model in single schema mode
        if self.is_single_schema_mode and model is not None:
            raise ValueError('The model argument is not allowed in single schema mode')

        # You must specify a valid model in multi schema mode
        if not self.is_single_schema_mode:
            if model is None:
                raise ValueError('The model argument is required in multi schema mode')
            if model not in self.schema.values():
                raise ValueError(f"Model of type {type(model)} isn't part of this connection's schema")

        # Store the row in the staging area
        self.records[model or self.schema].append(item)
        self.counter += 1

        # Flush in batches if we need to
        if self.counter >= self.AUTO_FLUSH_TRIGGER:
            self.flush()

    def flush(self):
        """
        Send pending data changes to the database.
        """
        if self.records:
            # This is a no-op if we're already connected
            self.connect()

            # Flush all the records to the DB using fast batched inserts
            for model, values in self.records.items():
                # Ensure all the records have the same keys since they share an insert statement
                first, *remaining = values
                first_keys = first.keys()
                for item in remaining:
                    if item.keys() != first_keys:
                        raise ValueError(
                            'Record keys inconsistent during flush, ensure all records have the same key '
                            f"when using add, expected {list(first_keys)}, found {list(item.keys())}"
                        )

                # Automatically handle duplicates (only supported on PostgreSQL)
                if self.ignore_duplicates:
                    statement = postgresql.insert(model.__table__).on_conflict_do_nothing()
                else:
                    statement = model.__table__.insert()

                # Send the batch of records to the database
                self.engine.execute(statement, values)

            # Reset the staging area ready for more records
            self.records.clear()
            self.counter = 0
